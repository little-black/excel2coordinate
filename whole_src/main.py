import sys
from PyQt5.QtWidgets import QApplication, QMainWindow
from init import *

if __name__ == '__main__':
    app = QApplication(sys.argv)

    mainWindow = myUI()
    mainWindow.show()

    sys.exit(app.exec_())
