# -*- coding: utf-8 -*-

from gui import *
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog, QGraphicsScene, QGraphicsView
from PyQt5.QtWidgets import QMessageBox
import xlrd
import pandas as pd
import matplotlib.pyplot as plt
from figure import *
from digit import *
matplotlib.use("Qt5Agg")

data = 0
table = 0
buf0 = []
buf1 = []
LEN = 0
state = 0
csvdata = 0
class myUI(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(QMainWindow, self).__init__()
        self.setupUi(self)
        self.myConnect()

    def myConnect(self):
        self.open_xls.triggered.connect(self.openExcel)
        self.open_cvs.triggered.connect(self.openCvs)
        self.X.currentIndexChanged.connect(self.axis1Change)
        self.Y.currentIndexChanged.connect(self.axis1Change)
        self.X_2.currentIndexChanged.connect(self.axis2Change)
        self.Y_2.currentIndexChanged.connect(self.axis2Change)

    def msg(self,message):
        reply = QMessageBox.information(self,
                                    "Error",
                                    message,
                                    QMessageBox.Yes)

    def openExcel(self):
        global data
        global LEN
        global table
        global state
        try:
            excelname = QFileDialog.getOpenFileName(self, "CHOOSE FILE", "./")
            data = xlrd.open_workbook(excelname[0])
        except:
            print("error")
            self.msg('not a excel file')
        else:
            state = 1
            table = data.sheets()[0]
            LEN = table.nrows
            self.setComBox(table.row_values(0))

    def openCvs(self):
        global state
        global csvdata
        try:
            csvname = QFileDialog.getOpenFileName(self, "CHOOSE FILE", "./")
            csvdata = pd.read_csv(csvname[0])
        except:
            print('error')
            self.msg('not a csv file')
        else:
            state = 2
            axisList = []
            for x in csvdata:
                axisList.append(x)
            self.setComBox(axisList=axisList)

    def axis2Change(self):
        global buf0
        global table
        global LEN
        global state
        global csvdata
        if state == 1:
            indexX = self.X_2.currentIndex()
            indexY = self.Y_2.currentIndex()
            listX = table.col_values(indexX)
            listY = table.col_values(indexY)
            listX,listY = check(listX, listY)
            dr = Figure_Canvas()
            dr.mydraw(listX,listY)
            graphicscene = QtWidgets.QGraphicsScene()
            graphicscene.addWidget(dr)
            self.graphicsView_2.setScene(graphicscene)
            self.graphicsView_2.show()
        elif state == 2:
            listX = []
            listY = []
            axisName = self.X_2.currentText()
            if axisName!='':
                for x in csvdata[axisName]:
                    listX.append(x)
            axisName = self.Y_2.currentText()
            if axisName!='':
                for x in csvdata[axisName]:
                    listY.append(x)
            if len(listX)!=0 and len(listY)!=0:
                listX,listY = check(listX, listY)
                dr = Figure_Canvas()
                dr.mydraw(listX,listY)
                graphicscene = QtWidgets.QGraphicsScene()
                graphicscene.addWidget(dr)
                self.graphicsView_2.setScene(graphicscene)
                self.graphicsView_2.show()
            else:
                print('error')

    def axis1Change(self):
        global buf0
        global table
        global LEN
        global state
        global csvdata
        if state == 1:
            indexX = self.X.currentIndex()
            indexY = self.Y.currentIndex()
            listX = table.col_values(indexX)
            listY = table.col_values(indexY)
            #非数字数据会使程序卡死,check删除不合法点
            listX,listY = check(listX, listY)
            #Figure_Canvas是连接matplotlib和Qt的关键
            dr = Figure_Canvas()
            dr.mydraw(listX,listY)
            graphicscene = QtWidgets.QGraphicsScene()
            graphicscene.addWidget(dr)
            self.graphicsView.setScene(graphicscene)
            self.graphicsView.show()
        elif state == 2:
            listX = []
            listY = []
            axisName = self.X.currentText()
            if axisName!='':
                for x in csvdata[axisName]:
                    listX.append(x)
            axisName = self.Y.currentText()
            if axisName!='':
                for x in csvdata[axisName]:
                    listY.append(x)
            if len(listX)!=0 and len(listY)!=0:
                listX,listY = check(listX, listY)
                dr = Figure_Canvas()
                dr.mydraw(listX,listY)
                graphicscene = QtWidgets.QGraphicsScene()
                graphicscene.addWidget(dr)
                self.graphicsView.setScene(graphicscene)
                self.graphicsView.show()
            else:
                print('error')

    def setComBox(self,axisList):
        self.X.clear()
        self.Y.clear()
        self.X_2.clear()
        self.Y_2.clear()
        self.X.addItems(axisList)
        self.Y.addItems(axisList)
        self.X_2.addItems(axisList)
        self.Y_2.addItems(axisList)
