summary:
this is a easy tool to read point in excel or cvs and draw them at a coordinate.
BUT THE FILE FORMAT MUST BE THE SAME AS ./testdata
use python 3.6 and pyqt5
need python package:pyqt5  xlrd  pandas  matplotlib
===============================================================================
usage:
cd whole_src
python main.py
===============================================================================
feature:
in this project i insert a matplotlib to qt graphview
===============================================================================
where to improve:
1.maybe can use Qt paint directly draw in weight.
===============================================================================
REMARK:
I use pyinstaller to pack this program in win10,this command is successfully,
but main.exe can not be work.
Error about pandas c extension problem.
Finally i find a crafty way  = .=
command as follow:
pyinstaller main.py --hidden-import=pandas._libs.tslibs.timedeltas
===============================================================================
if meet problems about this program or have some suggestions ,please e-mail me!
THANKS!!

author:L-BLACK
mail:rgb000000black@gmail.com
